LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;

ENTITY CORDIC_PIPE IS
	PORT (
			CLOCK	: IN  std_logic;								-- Taktsignal
			X_in 	: IN  signed(15 DOWNTO 0);						-- Eingabewert für X
			Y_in	: IN  signed(15 DOWNTO 0) := (OTHERS => '0');	-- Eingabewert für Y und Initialisierung mit '0'
			Z_in 	: IN  signed(15 DOWNTO 0);						-- Eingabewert für Z
			X_out 	: OUT signed(15 DOWNTO 0);						-- Ausgabewert für X
			Y_out 	: OUT signed(15 DOWNTO 0)						-- Ausgabewert für Y
	);
END CORDIC_PIPE;

ARCHITECTURE BEHAVIOR OF CORDIC_PIPE IS
   -- Definition of data types
   TYPE XY_vector IS ARRAY(16 DOWNTO 0) OF SIGNED(15 DOWNTO 0);
   TYPE Z_vector IS ARRAY(16 DOWNTO 0) OF SIGNED(19 DOWNTO 0);

   -- Declaration of the component CORDIC_ELEMENT
   COMPONENT CORDIC_ELEMENT IS
   GENERIC (
      STUFE   : NATURAL := 1
   );
   PORT (
      CLOCK   : IN STD_LOGIC;                 -- Clock signal
      X_in    : IN SIGNED(15 DOWNTO 0);      -- Input value for X (16 Bit)
      Y_in    : IN SIGNED(15 DOWNTO 0);      -- Input value for Y (16 Bit)
      Z_in    : IN SIGNED(19 DOWNTO 0);      -- Input value for Z (20 Bit)
      X_out   : OUT SIGNED(15 DOWNTO 0);     -- Output value for X
      Y_out   : OUT SIGNED(15 DOWNTO 0);     -- Output value for Y
      Z_out   : OUT SIGNED(19 DOWNTO 0)      -- Output value for Z
   );
	END COMPONENT;

   -- Internal signals
   SIGNAL X, Y : XY_vector;
   SIGNAL Z : Z_vector;

BEGIN
   -- Filling the first node
   X(0) <= (others => '0');
   Y(0) <= (others => '0');
   Z(0)(19 DOWNTO 4) <= (others => '0');
   Z(0)(3 DOWNTO 0) <= (others => '0');

   -- Forming the pipeline
   PIPE_GEN: FOR n IN 1 TO 16 GENERATE
      CE: CORDIC_ELEMENT
      GENERIC MAP (STUFE => n-1)
      PORT MAP (
         CLOCK => CLOCK,
         X_in => X(n-1),
         Y_in => Y(n-1),
         Z_in => Z(n-1),
         X_out => X(n),
         Y_out => Y(n),
         Z_out => Z(n)
      );
   END GENERATE PIPE_GEN;

   -- Transferring the values to the output ports
   PROCESS(CLOCK)
   BEGIN
      IF (CLOCK'event AND CLOCK = '1') THEN
         -- Limiter at 0Â°, if cos tilts to -1
         IF (X(16)(15) = '1') THEN
            X_out <= X"7FFF";
            Y_out <= X"0000";
         -- Limiter at 90Â° if sin tilts to -1
         ELSIF (Y(16)(15) = '1') THEN
            X_out <= X"0000";
            Y_out <= X"7FFF";
         ELSE
            -- Handle other cases as needed
            -- For example, just pass through the last result
            X_out <= X(16);
            Y_out <= Y(16);
         END IF;
      END IF;
   END PROCESS;

END BEHAVIOR;
