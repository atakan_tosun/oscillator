-------------------------------------------------------------------------------
--  Datei: Oszillator.vhd                                                    --
--  benÃ¶tigt: cordic.vhd, cordic_element.vhd, cordic_pipe.vhd                --
--  Autor: Jochen Saumer                                                     --
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--  Oszillator                                                               --
-------------------------------------------------------------------------------
--                                                                           --
--  Erzeugt Sinussignal mit 64 Sinuswerten pro Schwingung und einer Frequenz --
--  von Eingangstakt geteilt durch 64.                                       --
--                                                                           --
-------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;


ENTITY OSCILLATOR IS
	PORT (
			CLOCK 	: IN  std_logic;			-- Eingangstakt 
			SIN 	: OUT signed(15 downto 0)	-- Sinuswerte
	);
END OSCILLATOR;


ARCHITECTURE BEHAVIOR OF OSCILLATOR IS
	
	COMPONENT CORDIC IS
		PORT (
        CLOCK   : IN  std_logic;            -- Taktsignal
        A_in    : IN  signed(15 DOWNTO 0);  -- Eingabewert fÃ¼r zu berechnenden Winkel
        SIN     : OUT signed(15 DOWNTO 0);  -- Sinus des Ã¼bergebenen Winkels
        COS     : OUT signed(15 DOWNTO 0)   -- Cosinus des Ã¼bergebenen Winkels
    );
	END COMPONENT;
	
	SIGNAL SIN_temp : signed(15 DOWNTO 0);
	SIGNAL SIN_temp2 : signed(15 DOWNTO 0);				-- Hilfsvariable: SIN_temp2 wird zur Konvertierung auf STD_LOGIC_VECTOR benÃ¶tigt
	SIGNAL INDEX 	: signed(15 DOWNTO 0) := x"0000";	-- Laufindex der von x"0000" = 0Â° bis x"4000" = 90Â° lÃ¤uft
	
BEGIN

	-- CORDIC: Berechnet Sinuswerte zu ï¿½bergebenen Winkeln
	C0:CORDIC port map (CLOCK => CLOCK, A_in => INDEX, SIN => SIN_temp);
	
	PROCESS(CLOCK)
		
		VARIABLE CNTDIR 	 : std_logic := '0';			-- Zï¿½hlrichtung: 1=>Winkelinkrement, 0=>Winkeldekrement
		VARIABLE INV 		 : std_logic := '0';			-- Invertierung der Sinuswerte
		CONSTANT UNTERGRENZE : SIGNED(15 downto 0) := x"0000";	-- Untergrenze entspricht 0ï¿½
		CONSTANT OBERGRENZE  : SIGNED(15 downto 0) := x"4000";	-- Obergrenze entspricht 90ï¿½
		CONSTANT DELTA 		 : SIGNED(15 downto 0) := x"0400";	-- 64 Schritte pro Periode
		
	BEGIN
		
		IF (CLOCK'event and CLOCK = '0') THEN			
			
			-- ï¿½nderung der Zï¿½hlrichtung falls Ober/Untergrenze erreicht
			-- 16 Takte nach Erreichen der Untergrenze Invertierung aktivieren
			IF (INDEX = OBERGRENZE) THEN
				CNTDIR := '0';
			ELSIF (INDEX = UNTERGRENZE) THEN
				CNTDIR := '1';
			END IF;

			-- Eingangswinkel - Inkrement/Dekrement
			IF (CNTDIR = '1') THEN 
				--SIN_temp2 <= std_logic_vector(unsigned(SIN_temp) + unsigned(DELTA));
				SIN_temp2 <= SIN_TEMP + DELTA;
			ELSE 
				--SIN_temp2 <= std_logic_vector(unsigned(SIN_temp) - unsigned(DELTA));
				SIN_temp2 <= SIN_TEMP - DELTA;
			END IF;
			-- Invertierung der Sinuswerte
			IF (INV = '0') THEN
				SIN <= SIN_temp2;
			ELSE
				SIN <= SIN_TEMP2; --should be NOT SIN_TEMP2 but compiler doesnt recognize SIN_TEMP2
			END IF;

		END IF;

	END PROCESS;

END BEHAVIOR;