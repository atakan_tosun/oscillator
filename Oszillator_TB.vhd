library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity TESTBENCH is
end TESTBENCH;

architecture VERHALTEN of TESTBENCH is

	component OSCILLATOR IS
	PORT (
			CLOCK 	: IN  std_logic;			-- Eingangstakt 
			SIN 		: OUT signed(15 downto 0)	-- Sinuswerte
	);
	END component;
	
	constant T: time := 20 ns;
	signal CLK: std_logic := '0';
	signal SIN: signed(15 downto 0);
	
begin

	DUT: OSCILLATOR port map(CLOCK=>CLK, SIN=>SIN);
	
	CLK <= not CLK after T/2;

end VERHALTEN;