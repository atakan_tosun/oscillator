-------------------------------------------------------------------------------
--  Datei: Cordic.vhd                                                        --
--  ben�tigt: Cordic_Element.vhd, Cordic_Pipe.vhd                            --
--  Autor: Jochen Saumer                                                     --
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--  CORDIC - Algorithmus                                                     --
-------------------------------------------------------------------------------
--                                                                           --
--  Berechnet Sinus und Cosinus Werte f�r �bergebenen Winkel                 --
--                                                                           --
-------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_arith.ALL;


ENTITY CORDIC IS
	PORT (
			CLOCK	: IN  std_logic;			-- Taktsignal
			A_in 	: IN  signed(15 DOWNTO 0);	-- Eingabewert für zu berechnenden Winkel
			SIN		: OUT signed(15 DOWNTO 0);	-- Sinus des übergebenen Winkels
			COS		: OUT signed(15 DOWNTO 0) 	-- Cosinus des übergebenen Winkels
	);
END CORDIC;


ARCHITECTURE BEHAVIOR OF CORDIC IS
		
	COMPONENT CORDIC_PIPE IS
		PORT (
			CLOCK	: IN  std_logic;								-- Taktsignal
			X_in 	: IN  signed(15 DOWNTO 0);						-- Eingabewert für X
			Y_in	: IN  signed(15 DOWNTO 0) := (OTHERS => '0');	-- Eingabewert für Y und Initialisierung mit '0'
			Z_in 	: IN  signed(15 DOWNTO 0);						-- Eingabewert für Z
			X_out 	: OUT signed(15 DOWNTO 0);						-- Ausgabewert für X
			Y_out 	: OUT signed(15 DOWNTO 0)						-- Ausgabewert für Y
	);
	END COMPONENT;
	
	-- Definition der Skalierungskonstanten P_unskaliert=1/k (k = 0.6073 => P_unskaliert = 1.6467)
	-- P = 2^15 * P_unskaliert = 19898(dec) = 4DBA(hex)
	CONSTANT P : signed(15 DOWNTO 0) := x"4dba";

BEGIN
	
	PIPE: CORDIC_PIPE PORT MAP (
        CLOCK => CLOCK,
        X_in => P,          -- X wird mit der Konstanten P initialisiert
        Y_in => (OTHERS => '0'), -- Y wird mit Nullen initialisiert
        Z_in => A_in,   -- Z wird mit dem skalierten Winkel initialisiert
        X_out => SIN,  -- Das Ergebnis für Cosinus wird in COS_data gespeichert
        Y_out => COS   -- Das Ergebnis für Sinus wird in SIN_data gespeichert
    );

END BEHAVIOR;