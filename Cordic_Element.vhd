LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;

ENTITY CORDIC_ELEMENT IS
   GENERIC (
		STUFE   : NATURAL := 1                -- Stage in the iteration chain
   );
   PORT (
      CLOCK   : IN STD_LOGIC;                 -- Clock signal
      X_in    : IN SIGNED(15 DOWNTO 0);      -- Input value for X (16 Bit)
      Y_in    : IN SIGNED(15 DOWNTO 0);      -- Input value for Y (16 Bit)
      Z_in    : IN SIGNED(19 DOWNTO 0);      -- Input value for Z (20 Bit)
      X_out   : OUT SIGNED(15 DOWNTO 0);     -- Output value for X
      Y_out   : OUT SIGNED(15 DOWNTO 0);     -- Output value for Y
      Z_out   : OUT SIGNED(19 DOWNTO 0)      -- Output value for Z
   );
END CORDIC_ELEMENT;

ARCHITECTURE BEHAVIOR OF CORDIC_ELEMENT IS
   -- Function CONST_ARCTAN
   -- This is a lookup table containing precalculated arctan values.
   -- 'n' is the level of the CORDIC element and returns a 20-bit arctan value.
   -- The values are calculated as follows: Z(n) = atan(1/2^n)
   -- 20 Bit values => 2^20 = 2pi (rad) => 1 (rad) = 2^20/2pi = 166886.053...
   -- n=0: atan(1/1) * 2^20/2pi = 131072.00(dec) = 20000(hex)
   -- n=1: atan(1/2) * 2^20/2pi = 77376.32(dec) = 12E40(hex)
   FUNCTION CONST_ARCTAN(n : NATURAL) RETURN SIGNED IS
      VARIABLE result : SIGNED(19 downto 0);
   BEGIN
      CASE n IS
         WHEN 0 	=> result := x"20000";
			WHEN 1 	=> result := x"12E40";
			WHEN 2 	=> result := x"09FB4";
			WHEN 3	=> result := x"05111";
			WHEN 4	=> result := x"028B1";
			WHEN 5	=> result := x"0145D";
			WHEN 6	=> result := x"00A2F";
			WHEN 7	=> result := x"00518";
			WHEN 8	=> result := x"0028C";
			WHEN 9	=> result := x"00146";
			WHEN 10	=> result := x"000A3";
			WHEN 11	=> result := x"00051";
			WHEN 12	=> result := x"00029";
			WHEN 13	=> result := x"00020";
			WHEN 14	=> result := x"0000A";
			WHEN 15	=> result := x"00005";
         WHEN OTHERS => result := x"00000";
      END CASE;
      RETURN result;
   END FUNCTION CONST_ARCTAN;

   -- Function SHIFT_RIGHT
   -- Functionality of an Arithmetic Right Shifter
   -- Shift ARG to COUNT positions to the right
   FUNCTION SHIFT_RIGHT(ARG : SIGNED; COUNT : NATURAL) RETURN SIGNED IS 
      VARIABLE retval : SIGNED(ARG'range);
   BEGIN
      FOR n IN ARG'high DOWNTO ARG'high - COUNT + 1 LOOP
         retval(n) := ARG(ARG'high);
      END LOOP;
      
      FOR n IN ARG'high - COUNT DOWNTO 0 LOOP
         retval(n) := ARG(n + COUNT);
      END LOOP;

      RETURN retval;
   END FUNCTION SHIFT_RIGHT;

   -- Function ADD_SUB
   -- Depending on the argument ADDSUB, the function adds/subtracts DATA1 and DATA2
   FUNCTION ADD_SUB(DATA1, DATA2 : SIGNED; ADDSUB : STD_LOGIC) RETURN SIGNED IS 
   BEGIN
      IF (ADDSUB = '1') THEN
         RETURN DATA1 + DATA2;
      ELSE
         RETURN DATA1 - DATA2;
      END IF;
   END ADD_SUB;

   -- Internal signals
   SIGNAL dX       : SIGNED(15 DOWNTO 0);      -- Delta X (shifted for iteration)
   SIGNAL X_result : SIGNED(15 DOWNTO 0);      -- Iterated X value
   SIGNAL dY       : SIGNED(15 DOWNTO 0);      -- Delta Y (shifted for iteration)
   SIGNAL Y_result : SIGNED(15 DOWNTO 0);      -- Iterated Y value
   SIGNAL ARCTAN   : SIGNED(19 DOWNTO 0);      -- arctan value for the current level
   SIGNAL Z_result : SIGNED(19 DOWNTO 0);      -- Iterated Z value
   SIGNAL Z_neg, Z_pos : STD_LOGIC;            -- Sign bit of Z

BEGIN
   -- Calculation of ARCTAN for the current stage
   ARCTAN <= CONST_ARCTAN(STUFE);

   -- Value assignment of Zneg and Zpos
   Z_neg <= Z_in(19);
   Z_pos <= NOT Z_neg;

   -- Iteration of X (X_result = X_in - Y_in/2^n)
   X_result <= ADD_SUB(X_in, SHIFT_RIGHT(Y_in, STUFE), Z_neg);

   -- Iteration of Y (Y_result = Y_in + X_in/2^n)
   Y_result <= ADD_SUB(Y_in, SHIFT_RIGHT(X_in, STUFE), Z_pos);

   -- Iteration of Z (Z_result = Z_in - ARCTAN)
   Z_result <= ADD_SUB(Z_in, ARCTAN, Z_neg);

   -- Process for transferring the results to the output ports
   PROCESS(CLOCK)
   BEGIN
      IF (CLOCK'event and CLOCK = '1') THEN
         X_out <= X_result;
         Y_out <= Y_result;
         Z_out <= Z_result;
      END IF;
   END PROCESS;

END BEHAVIOR;
